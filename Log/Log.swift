//
//  Log.swift
//  Log
//
//  Created by Izzy Fraimow on 5/20/15.
//  Copyright (c) 2015 Izzy Fraimow. All rights reserved.
//

import Foundation

public enum LogLevel {
    case Emergency
    case Alert
    case Critical
    case Error
    case Warning
    case Notice
    case Info
    case Debug

    public func toString() -> String {
        switch self {
        case .Emergency:
            return "Emergency"
        case .Alert:
            return "Alert"
        case .Critical:
            return "Critical"
        case .Error:
            return "Error"
        case .Warning:
            return "Warning"
        case .Notice:
            return "Notice"
        case .Info:
            return "Info"
        case .Debug:
            return "Debug"
        }

    }
}

public struct LogChannel {
    public let identifier : String
    /// If the current `logger` supports writing to multiple files `backingStore` specifies what file to create and write to
    public let backingStore : NSURL?

    public init(identifier newIdentifier: String, backingStore newBackingStore: NSURL? = nil) {
        self.identifier = newIdentifier
        self.backingStore = newBackingStore
    }
}

public struct LogLocation {
    public let module: String
    public let file: String
    public let line: Int
}

public struct LogRecord {
    public let level: LogLevel
    public let location: LogLocation
    public let channel: LogChannel
    public let message: String
}

public protocol Logger {
    func enabled() -> Bool
    func log(record: LogRecord)
}

public struct Log {
    private static let DefaultIdentifier = NSBundle.mainBundle().bundleIdentifier ?? "com.anathemacalculus.log"
    private static let LogChannelDefault = LogChannel(identifier: DefaultIdentifier)

    private static var _logger: Logger?
    private static var logger: Logger {
        get {
            if _logger == nil {
                _logger = ASLLogger()
            }
            return _logger!
        }
    }
    
    public static func setLogger(newLogger: Logger) {
        _logger = newLogger
    }
    
    public static func emergency(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Emergency, channel: channel, file: file, line: line)
    }
    
    public static func alert(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Alert, channel: channel, file: file, line: line)
    }
    
    public static func critical(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Critical, channel: channel, file: file, line: line)
    }
    
    public static func error(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Error, channel: channel, file: file, line: line)
    }
    
    public static func warning(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Warning, channel: channel, file: file, line: line)
    }
    
    public static func notice(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Notice, channel: channel, file: file, line: line)
    }
    
    public static func info(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Info, channel: channel, file: file, line: line)
    }
    
    public static func debug(message : String, channel : LogChannel = LogChannelDefault, file: String = __FILE__, line: Int = __LINE__) {
        Log.log(message, level: .Debug, channel: channel, file: file, line: line)
    }
    
    private static func log(message : String, level: LogLevel, channel : LogChannel, file: String, line: Int) {
        let location = LogLocation(module: "TBD", file: file, line: line)
        let record = LogRecord(level: level, location: location, channel: channel, message: message)
        logger.log(record)
    }
}
