//
//  Log.h
//  Log
//
//  Created by Izzy Fraimow on 6/1/15.
//  Copyright (c) 2015 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "asl.h"

//! Project version number for Log.
FOUNDATION_EXPORT double LogVersionNumber;

//! Project version string for Log.
FOUNDATION_EXPORT const unsigned char LogVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Log/PublicHeader.h>


