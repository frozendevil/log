//
//  ViewController.swift
//  LogExample
//
//  Created by Izzy Fraimow on 7/3/15.
//  Copyright (c) 2015 anathemacalculus. All rights reserved.
//

import Cocoa
import Log
import SQLiteLogger

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        Log.debug("hello")
        Log.critical("!!!HELLO!!!")

        let desktopURL = NSURL(string: "~/Desktop/test.sqlite")
        let channel = LogChannel(identifier: "com.anathemacalculus.sqlite-test", backingStore: desktopURL)

        Log.setLogger(SQLiteLogger())
        Log.debug("hello world", channel: channel)

        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

