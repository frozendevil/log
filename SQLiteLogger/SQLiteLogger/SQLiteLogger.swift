//
//  SQLiteLogger.swift
//  SQLiteLogger
//
//  Created by Izzy Fraimow on 7/5/15.
//  Copyright (c) 2015 anathemacalculus. All rights reserved.
//

import Foundation
import Log
import sqlite3

public class SQLiteLogger: Logger {
    private var databasePool: [String: COpaquePointer] = Dictionary()

    public init() {

    }

    deinit {
        for (fileURLString, database) in databasePool {
            if sqlite3_close(database) != SQLITE_OK {
                println("error closing database")
            }
        }
    }

    public func enabled() -> Bool {
        return true
    }

    private func databaseForChannel(channel: LogChannel) -> COpaquePointer? {
        let fileURLString : String?
        if let backingStoreURL = channel.backingStore {
            fileURLString = backingStoreURL.absoluteString?.stringByExpandingTildeInPath
        } else {
            let documentsSearchPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as? String
            fileURLString = documentsSearchPath?.stringByAppendingPathComponent("log.sqlite")
        }


        if let fileURLString = fileURLString {
            if let storedDatabase = databasePool[fileURLString] {
                return storedDatabase
            }

            var database: COpaquePointer = nil
            if sqlite3_open(fileURLString, &database) != SQLITE_OK {
                return nil
            }

            return database
        }

        return nil
    }

    private let SQLITE_STATIC = sqlite3_destructor_type(COpaquePointer(bitPattern: 0))      // http://stackoverflow.com/a/26884081/1271826
    private let SQLITE_TRANSIENT = sqlite3_destructor_type(COpaquePointer(bitPattern: -1))

    let DATABASE_SCHEMA = "create table if not exists log (id INTEGER primary key autoincrement, channel TEXT, time TEXT, level TEXT, module TEXT, file TEXT, line TEXT, message TEXT)"
    let INSERT_FORMAT = "insert into log (channel, time, level, module, file, line, message) values (?, ?, ?, ?, ?, ?, ?)"

    private func initalizeDatabase(database: COpaquePointer) {
        if sqlite3_exec(database, DATABASE_SCHEMA, nil, nil, nil) != SQLITE_OK {
            let errmsg = String.fromCString(sqlite3_errmsg(database))
            println("error creating table: \(errmsg)")
        }
    }

    private func prepareLogInsertStatement(database: COpaquePointer) -> COpaquePointer {
        var statement: COpaquePointer = nil

        if sqlite3_prepare_v2(database, INSERT_FORMAT, -1, &statement, nil) != SQLITE_OK {
            let errmsg = String.fromCString(sqlite3_errmsg(database))
            println("error preparing insert: \(errmsg)")
        }

        return statement
    }

    private func bindStatement(statement: COpaquePointer, toValues record: LogRecord) {
        sqlite3_reset(statement)

        let values : [String] = [record.channel.identifier, "\(NSDate.timeIntervalSinceReferenceDate())", record.level.toString(), record.location.module, record.location.file, "\(record.location.line)", record.message]

        let parameterCount = sqlite3_bind_parameter_count(statement)

        if Int32(values.count) != parameterCount {
            println("Error binding parameters")
        }

        for index in 1...parameterCount {
            let text = values[index - 1]
            if sqlite3_bind_text(statement, CInt(index), text, -1, SQLITE_TRANSIENT) != SQLITE_OK {
                println("Error binding parameters")
                sqlite3_finalize(statement)
            }
        }
    }

    private func commitToLog(statement: COpaquePointer, database: COpaquePointer) {
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String.fromCString(sqlite3_errmsg(database))
            println("failure inserting null: \(errmsg)")
        }
    }

    public func log(record: LogRecord) {
        let maybeDatabase = databaseForChannel(record.channel)

        if maybeDatabase == nil {
            println("error creating database")
        }

        let database = maybeDatabase!

        initalizeDatabase(database)

        let instertStatement = prepareLogInsertStatement(database)

        bindStatement(instertStatement, toValues: record)

        commitToLog(instertStatement, database: database)
    }
}