//
//  AppDelegate.swift
//  LogExample
//
//  Created by Izzy Fraimow on 7/3/15.
//  Copyright (c) 2015 anathemacalculus. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

