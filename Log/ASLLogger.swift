//
//  ASLLogger.swift
//  Log
//
//  Created by Izzy Fraimow on 6/1/15.
//  Copyright (c) 2015 Anathema Calculus. All rights reserved.
//

import Foundation

public class ASLLogger: Logger {
    private static let ASL_FLAGS = ASL_OPT_CREATE_STORE | ASL_OPT_OPEN_WRITE | ASL_OPT_STDERR
    private var clients : [String : asl_object_t] = Dictionary()
    
    deinit {
        for (identifier, client) in clients {
            asl_close(client)
        }
    }

    public func enabled() -> Bool {
        return true
    }

    public func log(record: LogRecord) {
        let level = ASLLevelForLogLevel(record.level)
        let client = ASLClientForIdentifier(record.channel.identifier)
        asl_vlog(client, nil, level, "\(record.message)", getVaList([]))
    }


    private func ASLClientForIdentifier(identifier: String) -> asl_object_t {
        if let client = clients[identifier] {
            return client
        } else {
            let client = asl_open(identifier, nil, numericCast(ASLLogger.ASL_FLAGS))
            clients[identifier] = client

            return client
        }
    }

    private func ASLLevelForLogLevel(level : LogLevel) -> Int32 {
        switch level {
        case .Emergency:
            return ASL_LEVEL_EMERG
        case .Alert:
            return ASL_LEVEL_ALERT
        case .Critical:
            return ASL_LEVEL_CRIT
        case .Error:
            return ASL_LEVEL_ERR
        case .Warning:
            return ASL_LEVEL_WARNING
        case .Notice:
            return ASL_LEVEL_NOTICE
        case .Info:
            return ASL_LEVEL_INFO
        case .Debug:
            return ASL_LEVEL_DEBUG
        }
    }
}